const messageQueue = {};

self.addEventListener('install', event => {
  console.log('[SW] Install event') ;
  event.waitUntil(self.skipWaiting());
});

self.addEventListener('activate', event => {
  console.log('[SW] Activate clients') ;
  event.waitUntil(self.clients.claim());
});

self.addEventListener('message', event => {
  const url = event.data.url;
  switch(event.data.type) {
  case 'send':
    messageQueue[event.data.messageId] = setTimeout(() => {
      console.log(`sended: ${event.data.text}` );
      event.source.postMessage({
        type: 'sended',
        messageId: event.data.messageId
      });
    }, 15000);
    break;
  case 'cancel':
    const taskId = messageQueue[event.data.messageId];
    delete messageQueue[event.data.messageId];
    console.log(`canceled: ${taskId}`);
    if (taskId) {
      clearTimeout(taskId);
      event.source.postMessage({
        type: 'canceled',
        messageId: event.data.messageId
      });
    } else {
      event.source.postMessage({
        type: 'failedToCancel',
        messageId: event.data.messageId
      });
    }
  }
});
