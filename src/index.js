const uuid = require('uuid');

if ('serviceWorker' in navigator) {
  navigator.serviceWorker.register('/sw.js')
    .then(registration => {
      return navigator.serviceWorker.ready;
    })
    .then(() => {
      navigator.serviceWorker.getRegistration()
        .then(registration => {
          console.table(registration);
        });

      if (navigator.serviceWorker.controller) {
        return Promise.resolve(navigator.serviceWorker.controller);
      }

      return new Promise(resolve => {
        navigator.serviceWorker.addEventListener('controllerchange', _ => {
          resolve(navigator.serviceWorker.controller);
        });
      });
    })
    .then(controller => {
      const event = new Event('sw-ready');
      document.dispatchEvent(event);
    });;
};

document.addEventListener('sw-ready', e => {
  console.log('sw-ready');
  navigator.serviceWorker.addEventListener('message', e => {
    switch(e.data.type) {
    case 'sended': {
      const tlEntry = document.getElementById(e.data.messageId);
      tlEntry.removeChild(tlEntry.querySelector("button"));
      tlEntry.querySelector('p').setAttribute('class', '');
      break;
    }
    case 'canceled': {
      const tlEntry = document.getElementById(e.data.messageId);
      document.getElementById("timeline").removeChild(tlEntry);
      break;
    }
    case 'failedToCancel':
      const tlEntry = document.getElementById(e.data.messageId);
      tlEntry.removeChild(tlEntry.querySelector("button"));
      tlEntry.querySelector('p').setAttribute('class', '');
      console.log('failed to cancel');
      break;
    }
  });
});

function sendMessage(message) {
  const channel = new MessageChannel();
  navigator.serviceWorker.controller.postMessage(message, [channel.port2]);
}

function commitMessage() {
  const text = document.getElementById('message').value || '';
  if (!text.trim()) return;
  const tlEntry = document.createElement('li');
  const messageId = uuid.v4();
  tlEntry.setAttribute('id', messageId);
  const tlMessage = document.createElement('p');
  tlMessage.innerText = text;
  tlMessage.setAttribute('class', 'blink');
  tlEntry.append(tlMessage);

  const cancelBtn = document.createElement('button');
  cancelBtn.setAttribute('class', 'btn cancel');
  cancelBtn.innerText = '×';
  cancelBtn.addEventListener('click', (e) => {
    sendMessage({
      type: 'cancel',
      messageId
    });
  });
  tlEntry.append(cancelBtn);

  document.getElementById("timeline").appendChild(tlEntry);
  tlEntry.scrollIntoView(false);
  sendMessage({
    type: 'send',
    messageId,
    text
  });
  document.getElementById("message").value = '';
}

document.getElementById("send").addEventListener('click', e => commitMessage());
document.getElementById("message").addEventListener('keypress', e => {
  if (e.keyCode == 13 && !e.shiftKey) {
    e.preventDefault();
    commitMessage();
  }
});
