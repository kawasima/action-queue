const path = require('path');
const ServiceWorkerWebpackPlugin = require('serviceworker-webpack-plugin');

module.exports = {
  mode: process.env.NODE_ENV || 'development',
  entry: ['./src/index.js'],
  output: {
    publicPath: '/',
    filename: 'bundle.js',
    path: __dirname + '/public'
  },
  plugins: [
    new ServiceWorkerWebpackPlugin({
      entry: path.join(__dirname, 'src/sw.js'),
      excludes: ['**/.*', '**/*.map', '*.html']
    })
  ]
};
