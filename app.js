const https = require('https');
const fs = require('fs');
const path = require('path');
const koa = require('koa');
const koaWebpack = require('koa-webpack');
const app = new koa();

const options = {
  key: fs.readFileSync('server.key'),
  cert: fs.readFileSync('server.cert')
};

(async () => {
  app.use(require('koa-static')('public'));
  const webpackMiddleware = await koaWebpack({
    configPath: path.join(__dirname, 'webpack.config.js')
  });
  app.use(webpackMiddleware);
  app.use(async ctx => {
    ctx.body = 'Hello world';
  });
  app.listen(3000);
//  https.createServer(options, app.callback()).listen(3000);
})();
